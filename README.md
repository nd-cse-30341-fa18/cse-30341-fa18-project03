# Project 03: Message Queue 

This is [Project 03] of [CSE.30341.FA18].

## Members

1. Domer McDomerson (dmcdomer@nd.edu)
2. Belle Fleur (bfleur@nd.edu)

## Demonstration

[Link to Demonstration Slides]()

## Errata

> Describe any known errors, bugs, or deviations from the requirements.

## Extra Credit

> Describe what extra credit (if any) that you implemented.

[Project 03]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa18/project03.html
[CSE.30341.FA18]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa18/
